package prants.bowlingScore.player;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    private IPlayer getTestClass() {
        return new Player();
    }

    @Test
    public void testPlayerNicknameAssign() {
        IPlayer testPlayer = getTestClass();
        IPlayer testPlayer2 = getTestClass();

        assertEquals(false, testPlayer.hasNickname("zero"), "Should not have nickname before adding it");

        testPlayer.addNickname("zero");

        assertEquals(true, testPlayer.hasNickname("zero"), "Should have nickname after adding it");
        assertEquals(false, testPlayer.hasNickname("one"), "Should not reply true to a wrong nickname");

        assertEquals(false, testPlayer2.hasNickname("zero"), "Different Player instances dont share same nickname pool");
    }

    @Test
    public void testPlayerCanAskForMainNickname() {
        IPlayer testPlayer = getTestClass();
        testPlayer.addNickname("zero");
        assertEquals("zero", testPlayer.getMainNickname(), "Should be able to get first nickname of the player as main nickname");
    }

    @Test
    public void testPlayerWithoutNicknamesHasNullAsMainNickname() {
        IPlayer testPlayer = getTestClass();
        assertEquals(null, testPlayer.getMainNickname(), "Should get null as main nickname, when player has no nicknames");
    }

}