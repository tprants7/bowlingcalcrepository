package prants.bowlingScore.game;

import org.junit.jupiter.api.Test;
import prants.bowlingScore.soloGame.ISoloGame;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private IGame getTestGame() {
        return new Game();
    }

    @Test
    public void testGameAddPlayer() {
        IGame testGame = getTestGame();
        assertEquals(true, testGame.addPlayer("first"), "Should be able to add a player");
    }

    @Test
    public void testGameCanAskForAddedPlayersNickname() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        testGame.addPlayer("second");
        assertEquals("second", testGame.getPlayerNickname(1), "Should be able to return the right persons nickname");
    }

    @Test
    public void testGameCanGiveAddedPlayerId() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        testGame.addPlayer("second");
        assertEquals(0, testGame.getPlayerId("first"), "Should be able to return an added players id");
    }

    @Test
    public void testGameExceptionWhenAskingForNonExistingPlayer() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        assertThrows(NullPointerException.class, () -> {
            testGame.getPlayerNickname(1);
        }, "Should throw NullPointerException when a player doesn't exist");
    }

    @Test
    public void testGameExceptionWhenUsingNonAddedNickname() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        assertThrows(NullPointerException.class, () -> {
            testGame.getPlayerId("second");
        }, "Should throw NullPointerException when a nickname isn't in use");
    }

    @Test
    public void testGameCantAddDuplicatePlayers() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        assertEquals(false, testGame.addPlayer("first"), "Should not be able to add player with same nickname again");
    }

    @Test
    public void testGameShouldAllowRollingForNewPlayer() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        assertEquals(true, testGame.canRollForPlayer(0), "Should accept rolls for new player");
        assertEquals(true, testGame.canRollForPlayer("first"), "Should accept rolls for new player");
    }

    @Test
    public void testGameShouldAccessRunningScoreChanges() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        assertEquals(0, testGame.getRunningScoreForPlayer(0), "Running score on start should be 0");
        assertEquals(0, testGame.getRunningScoreForPlayer("first"), "Running score on start should be 0");
        testGame.addPointsForPlayer(0, 3);
        testGame.addPointsForPlayer("first", 4);
        assertEquals(7, testGame.getRunningScoreForPlayer(0), "Running should change after first completed frame");
        assertEquals(7, testGame.getRunningScoreForPlayer("first"), "Running should change after first completed frame");
    }

    @Test
    public void testGameShouldGiveUpdatedFrameChanges() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        assertEquals(1, testGame.getPlayerCurrentFrame(0), "Player should start from frame 1");
        assertEquals(1, testGame.getPlayerCurrentFrame("first"), "Player should start from frame 1");
        testGame.addPointsForPlayer(0, 3);
        testGame.addPointsForPlayer("first", 4);
        assertEquals(2, testGame.getPlayerCurrentFrame(0), "Player should have moved to frame 2");
        assertEquals(2, testGame.getPlayerCurrentFrame("first"), "Player should have moved to frame 2");
    }

    @Test
    public void testGameShouldGiveRightAmountOfRemainingPinsForPlayer() {
        IGame testGame = getTestGame();
        testGame.addPlayer("first");
        testGame.addPlayer("second");
        assertEquals(10, testGame.getCurrentlyStandingPinsForPlayer(1), "New player should have 10 pins standing for him/her");
        testGame.addPointsForPlayer(1, 4);
        assertEquals(6, testGame.getCurrentlyStandingPinsForPlayer(1), "Standing pin number should change after a roll");

    }

    @Test
    public void testGameWillReturnPlayerCurrentStateDescription() {
        IGame testGame = this.getTestGame();
        testGame.addPlayer("first");
        assertEquals(true, testGame.getCurrentStateDescriptionForPlayer(0) != null, "Current state description element should not be null");
    }

    @Test
    public void testGameWillAddNewStateDescriptionsWhenMorePlayerAreAdded() {
        IGame testGame = this.getTestGame();
        assertEquals(true, testGame.getAllPlayersCurrentStates().isEmpty(), "List of all player states should be empty if there are no players");
        testGame.addPlayer("first");
        assertEquals(1, testGame.getAllPlayersCurrentStates().size(), "After adding one player amount of player states should match");
    }



}