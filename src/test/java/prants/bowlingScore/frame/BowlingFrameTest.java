package prants.bowlingScore.frame;

import org.junit.jupiter.api.Test;
import prants.bowlingScore.player.Player;
import prants.bowlingScore.soloGame.ISoloGame;
import prants.bowlingScore.soloGame.SoloGame;

import static org.junit.jupiter.api.Assertions.*;

class BowlingFrameTest {

    private IBowlingFrame getTestClass() {
        return new BowlingFrame();
    }

    private IBowlingFrame getTestFinalFrame() {
        return new BowlingLastFrame();
    }

    private ISoloGame getSoloGameTestClass() {
        return new SoloGame(new Player());
    }

    @Test
    public void testFrameNewFrameAllowsRolling() {
        IBowlingFrame testFrame = getTestClass();
        assertEquals(true, testFrame.canRollHere(), "New frame should allow rolling");
    }

    @Test
    public void testFrameFrameCanCalculateStandingPins() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(6);
        assertEquals(4, testFrame.getCurrentlyStandingPins(), "Frame should be able to calculate currently standing pins");
    }

    @Test
    public void testFrameStopsRollsAfterStrike() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(10);
        assertEquals(false, testFrame.canRollHere(), "After rolling a strike, frame should stop new rolling");
    }

    @Test
    public void testFrameContinuesRollsWithoutStrike() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(4);
        assertEquals(true, testFrame.canRollHere(), "If there isn't a strike, should allow a second throw");
    }

    @Test
    public void testFrameStopsRollsAfterSecondRoll() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(4);
        testFrame.addResult(5);
        assertEquals(false, testFrame.canRollHere(), "Normal frame should not allow rolls after second one");
    }

    @Test
    public void testFrameShowsFinalisedScoreAfter2RollsWithLessThen10Score() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(4);
        testFrame.addResult(5);
        assertEquals(true, testFrame.isScoreFinalised(), "Normal frame should not allow rolls after second one");
    }

    @Test
    public void testFrameGivesScoreEstimationWhenItsNotYetFinalised() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(5);
        assertEquals(false, testFrame.isScoreFinalised(), "Frame should not be finished after one less then 10 result");
        /*assertThrows(NullPointerException.class, () -> {
            testFrame.getScore();
        }, "Should throw a null pointer exception when asking for a score before it can be calculated");*/
        assertEquals(5, testFrame.getScore(), "If score is not yet finalised, should give an estimation of how many points have currently been given");

        testFrame.addResult(4);
        assertEquals(true, testFrame.isScoreFinalised(), "Frame should be finished after 10 rolls with a sum less then 10");
        assertEquals(9, testFrame.getScore(), "Frame should return a score of 9 if the sum of its two results is 9 pins");
    }

    @Test
    public void testFrameCanIdentifyAStrike() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(10);
        assertEquals(true, testFrame.isStrike(), "Frame can identify a strike");
        assertEquals(false, testFrame.isSpare(), "Frame doesn't think a strike is a spare");
    }

    @Test
    public void testFrameCanIdentifyASpare() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.addResult(4);
        testFrame.addResult(6);
        assertEquals(true, testFrame.isSpare(), "Frame can identify a spare");
        assertEquals(false, testFrame.isStrike(), "Frame doesn't think a spare is a strike");
    }

    @Test
    public void testFrameIsntFinalisedWhenNeedsExtraRolls() {
        IBowlingFrame testStrikeFrame = getTestClass();
        IBowlingFrame testSpareFrame = getTestClass();
        testStrikeFrame.addResult(10);
        testSpareFrame.addResult(6);
        testSpareFrame.addResult(4);
        assertEquals(false, testStrikeFrame.isScoreFinalised(), "Frame should not be finished if it cant calculate score for a strike");
        assertEquals(false, testSpareFrame.isScoreFinalised(), "Frame should not be finished if it cant calculate score for a spare");
    }

    @Test
    public void testFrameDefaultFinalFalueNeedsToBeFalse() {
        IBowlingFrame testFrame = getTestClass();
        assertEquals(false, testFrame.isFinalFrame(), "By default a new frame should not be final");
    }

    /*@Test
    public void testFrameCanSetAsFinalFrame() {
        IBowlingFrame testFrame = getTestClass();
        testFrame.setAsFinalValue(true);
        assertEquals(true, testFrame.isFinalFrame(), "Should be possible to assign a true value as finalFrame flag");
    }*/

    @Test
    public void testFrameFinalFrameChecksAsTrueFinal() {
        IBowlingFrame testFrame = getTestFinalFrame();
        assertEquals(true, testFrame.isFinalFrame(), "Test final frame needs to reply true to being finaö");
    }

    @Test
    public void testFrameCanAddSoloGameDependency() {
        IBowlingFrame testFrame = getTestClass();
        assertEquals(false, testFrame.hasSoloGameDependency(), "Should not have a solo game dependency before it is added");
        testFrame.addSoloGameDependency(this.getSoloGameTestClass());
        assertEquals(true, testFrame.hasSoloGameDependency(), "Should have a solo game dependency after it is added");
    }

    @Test
    public void testFrameDesignatedFinalFrameAllowsRollingAfterStrike() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(10);
        assertEquals(true, testFrame.canRollHere(), "Last frame should still allow rolling after strike");
    }

    @Test
    public void testFrameDesignatedFinalFrameAllowsRollingAfterSpare() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(5);
        testFrame.addResult(5);
        assertEquals(true, testFrame.canRollHere(), "Last frame should still allow rolling after spare");
    }

    @Test
    public void testFrameDesignatedFinalFrameStopsRollingAfter3ExtraRolls() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(10);
        testFrame.addResult(10);
        testFrame.addResult(5);
        assertEquals(false, testFrame.canRollHere(), "Last frame stop rolling after third roll");
    }

    @Test
    public void testFrameDesignatedFinalFrameAllowsOnly2RollsIfNoStrikeOrSpare() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(5);
        testFrame.addResult(4);
        assertEquals(false, testFrame.canRollHere(), "Last frame doesn't give extra shot without strike or spare");
    }

    @Test
    public void testFrameDesignatedFinalFrameCanSummarizeSpare() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(4);
        testFrame.addResult(6);
        testFrame.addResult(10);
        assertEquals(20, testFrame.getScore(), "Can summarize spare over last frame");
    }

    @Test
    public void testFrameDesignatedFinalFrameCanSummarizeStrike() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(10);
        testFrame.addResult(10);
        testFrame.addResult(5);
        assertEquals(25, testFrame.getScore(), "Can summarize strike over last frame");
    }

    @Test
    public void testFinalFrameExtraRoll10StandingPinSituation() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(10);
        testFrame.addResult(10);
        assertEquals(10, testFrame.getCurrentlyStandingPins(), "After two strikes in final frame, should have 10 pins for last roll");
    }

    @Test
    public void testFinalFrameExtraRollLessThen10StandingPinSituation() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(10);
        testFrame.addResult(6);
        assertEquals(4, testFrame.getCurrentlyStandingPins(), "After one strike and one non strike, should get accurate standing pin number");
    }

    @Test
    public void testFinalFrameExtraRollStandingPinsStrikeSituation() {
        IBowlingFrame testFrame = getTestFinalFrame();
        testFrame.addResult(4);
        testFrame.addResult(6);
        assertEquals(10, testFrame.getCurrentlyStandingPins(), "After one strike and one non strike, should get accurate standing pin number");
    }


}