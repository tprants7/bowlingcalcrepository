package prants.bowlingScore.frame;

import org.junit.jupiter.api.Test;
import prants.bowlingScore.player.IPlayer;
import prants.bowlingScore.player.Player;
import prants.bowlingScore.soloGame.ISoloGame;
import prants.bowlingScore.soloGame.SoloGame;

import static org.junit.jupiter.api.Assertions.*;

class BowlingFrameTestWithSoloGame {

    private ISoloGame getTestSoloGame() {
        return new SoloGame(getTestPlayer());
    }

    private IPlayer getTestPlayer() {
        return new Player();
    }

    private IBowlingFrame getTestFirstFrame(ISoloGame sourceGame) {
        return sourceGame.getFrameList().get(0);
    }

    private IBowlingFrame makesTestFrame() {
        return getTestFirstFrame(this.getTestSoloGame());
    }

    @Test
    public void testFrameShowsThatSpareFrameNotFinishedBeforeEnoughRollsLater() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(3);
        masterGame.rollHere(7);
        assertEquals(false, getTestFirstFrame(masterGame).isScoreFinalised(), "Spare Frame should not be finished before next frame has enough rolls");
    }

    @Test
    public void testFrameShowsThatSpareFrameIsFinishedAfterEnoughRollsLater() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(3);
        masterGame.rollHere(7);
        masterGame.rollHere(6);
        assertEquals(true, getTestFirstFrame(masterGame).isScoreFinalised(), "Spare Frame should be finished if next frame has one roll aswell");
    }

    @Test
    public void testFrameShowsProperScoreEstimationForUnfinishedSpare() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(3);
        masterGame.rollHere(7);
        assertEquals(10, getTestFirstFrame(masterGame).getScore(), "Spare frame estimation score should be 10 before extra points are rolled");
    }

    @Test
    public void testFrameShowsThatStrikeFrameNotFinishedBeforeEnoughRollsLater() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(10);
        masterGame.rollHere(7);
        assertEquals(false, getTestFirstFrame(masterGame).isScoreFinalised(), "Strike Frame should not be finished before next frame has enough rolls");
    }

    @Test
    public void testFrameShowsProperScoreEstimationForUnfinishedStrike() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(10);
        masterGame.rollHere(7);
        assertEquals(17, getTestFirstFrame(masterGame).getScore(), "Strike score estimation should be able to count if one of the extra strikes has been made");
    }

    @Test
    public void testFrameShowsThatStrikeFrameIsFinishedAfterEnoughRollsLater() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(10);
        masterGame.rollHere(7);
        masterGame.rollHere(2);
        assertEquals(true, getTestFirstFrame(masterGame).isScoreFinalised(), "Strike Frame should be finished if next frame has one roll aswell");
    }

    @Test
    public void testFrameCanCalculateScoreForSpare() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(3);
        masterGame.rollHere(7);
        masterGame.rollHere(6);
        assertEquals(16, getTestFirstFrame(masterGame).getScore(), "Spare Frame should be able to calculate score");
    }

    @Test
    public void testFrameCanCalculateScoreForStrike() {
        ISoloGame masterGame = getTestSoloGame();
        masterGame.rollHere(10);
        masterGame.rollHere(7);
        masterGame.rollHere(2);
        assertEquals(19, getTestFirstFrame(masterGame).getScore(), "Strike Frame should be able to calculate score");
    }

}