package prants.bowlingScore.frame;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FrameCurrentStateTest {

    private IBowlingFrame getTestClassFrame() {
        return new BowlingFrame();
    }

    private IFrameCurrentState getTestFrameState(IBowlingFrame testFrame, int frameNr) {
        return new FrameCurrentState(testFrame, frameNr);
    }

    @Test
    public void testStateStoresCurrentFrameScore() {
        IBowlingFrame testFrame = this.getTestClassFrame();
        testFrame.addResult(4);
        testFrame.addResult(5);
        IFrameCurrentState testFrameState = this.getTestFrameState(testFrame, 0);
        assertEquals(testFrame.getScore(), testFrameState.getCurrentFrameScore(), "Frame state should contain input frames total score");
    }

    @Test
    public void testStateStoresRollValues() {
        IBowlingFrame testFrame = this.getTestClassFrame();
        testFrame.addResult(4);
        testFrame.addResult(5);
        IFrameCurrentState testFrameState = this.getTestFrameState(testFrame, 0);
        assertEquals(testFrame.getAmountOfRolls(), testFrameState.getRollValues().size(),
                "Frames amount of rolls should match its registered states same value");
        assertEquals(testFrame.getAllResults().get(0).getKnockedDownPins(),
                testFrameState.getRollValues().get(0),
                "Frame knocked down pin count should match its states counterpart");
    }

    @Test
    public void testStateCanHaveFrameNrAssigned() {
        IBowlingFrame testFrame = this.getTestClassFrame();
        IFrameCurrentState testFrameState2 = this.getTestFrameState(testFrame, 2);
        IFrameCurrentState testFrameState3 = this.getTestFrameState(testFrame, 3);
        assertEquals(2, testFrameState2.getFrameNr(), "Assigned frame nr should be retrieved");
        assertEquals(3, testFrameState3.getFrameNr(), "Assigned frame nr should be retrieved");
    }

}