package prants.bowlingScore.result;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ResultTest {

    private IResult getFirstRollClass(int score) {
        return new Result(score);
    }

    private IResult getSecondaryRollClass(int score, int pinsAtStart) {
        return new Result(score, pinsAtStart);
    }

    @Test
    public void testResultCanConstructWithValue() {
        IResult testResult = getFirstRollClass(5);
        IResult testResult2 = getFirstRollClass(2);
        assertEquals(5, testResult.getKnockedDownPins(), "Assigned value should be saved");
        assertEquals(2, testResult2.getKnockedDownPins(), "Assigned value should be saved");
    }

    @Test
    public void testResultThrowsExceptionForOver10Score() {
        assertThrows(IllegalArgumentException.class, () -> {
            getFirstRollClass(11);
        }, "Should throw IllegalArgumentException when more then 10 pins are knocked down");
    }

    @Test
    public void testResultCanCalculateRemainingPinsFromFirstThrow() {
        IResult testResult = getFirstRollClass(5);
        IResult testResult2 = getFirstRollClass(2);
        assertEquals(5, testResult.getRemainingPins(),
                "Should be able to calculate remaining pins after 5/10 get knocked down");
        assertEquals(8, testResult2.getRemainingPins(),
                "Should be able to calculate remaining pins after 2/10 get knocked down");
    }

    @Test
    public void testResultCanConstructWithValueForSecondThrow() {
        IResult testResult = getSecondaryRollClass(6, 8);
        IResult testResult2 = getSecondaryRollClass(3, 4);
        assertEquals(6, testResult.getKnockedDownPins(), "Assigned value should be saved");
        assertEquals(3, testResult2.getKnockedDownPins(), "Assigned value should be saved");
    }

    @Test
    public void testResultThrowsExceptionForOver10ScoreOnSecondThrow() {
        assertThrows(IllegalArgumentException.class, () -> {
            getSecondaryRollClass(3, 2);
        }, "Should throw IllegalArgumentException when 3 are knocked down but only 2 pins remain");
    }

    @Test
    public void testResultCanCalculateRemainingPinsFromSecondThrow() {
        IResult testResult = getSecondaryRollClass(5, 5);
        IResult testResult2 = getSecondaryRollClass(2, 6);
        assertEquals(0, testResult.getRemainingPins(),
                "Should be able to calculate remaining pins after 5/5 get knocked down");
        assertEquals(4, testResult2.getRemainingPins(),
                "Should be able to calculate remaining pins after 2/6 get knocked down");
    }


}