package prants.bowlingScore.soloGame;

import org.junit.jupiter.api.Test;
import prants.bowlingScore.player.IPlayer;
import prants.bowlingScore.player.Player;

import static org.junit.jupiter.api.Assertions.*;

class SoloGameTest {

    private ISoloGame getTestClassObject() {
        return new SoloGame(this.getTestPlayer());
    }

    private IPlayer getTestPlayer() {
        return new Player();
    }

    @Test
    public void testSoloGameCanRollForANewGame() {
        ISoloGame testSoloGame = this.getTestClassObject();
        assertEquals(true, testSoloGame.needsRolls(), "New solo game should need rolling");
    }

    @Test
    public void testSoloGameHasAListOfTenFrames() {
        ISoloGame testSoloGame = this.getTestClassObject();
        assertEquals(10, testSoloGame.getFrameList().size(), "A bowling game should have 10 frames");
    }

    @Test
    public void testSoloGameStartsFromFirstFrame() {
        ISoloGame testSoloGame = this.getTestClassObject();
        assertEquals(testSoloGame.getCurrentFrame(), testSoloGame.getFrameList().get(0), "A new solo game should have first frame set as current frame");
    }

    @Test
    public void testSoloGameFrameShouldChangeToSecondOneAfterTwoStandardRolls() {
        ISoloGame testSoloGame = this.getTestClassObject();
        testSoloGame.rollHere(5);
        testSoloGame.rollHere(4);
        assertEquals(testSoloGame.getCurrentFrame(), testSoloGame.getFrameList().get(1), "After filling first frame, active frame should be second frame");
    }

    @Test
    public void testSoloGameFrameShouldChangeFrameIncaseStrike() {
        ISoloGame testSoloGame = this.getTestClassObject();
        testSoloGame.rollHere(10);
        assertEquals(testSoloGame.getCurrentFrame(), testSoloGame.getFrameList().get(1), "After filling first frame, active frame should be second frame");
    }

    @Test
    public void testSoloGameRollingShouldStopAfterLastFrameIsFinished() {
        ISoloGame testSoloGame = this.getTestClassObject();
        for(int i = 0; i < 9; i++) {
            testSoloGame.rollHere(10);
        }
        testSoloGame.rollHere(5);
        testSoloGame.rollHere(4);
        assertEquals(false, testSoloGame.needsRolls(), "After 10 frames finish, the game should not need more rolls");
    }

    @Test
    public void testSoloGameCanGetFinalStandindScoreWithoutStrikesAndSpares() {
        ISoloGame testSoloGame = this.getTestClassObject();

        testSoloGame.rollHere(4);
        testSoloGame.rollHere(5);
        assertEquals(9, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first frame");

        testSoloGame.rollHere(3);
        testSoloGame.rollHere(4);
        assertEquals(16, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first two frame");

        testSoloGame.rollHere(2);
        testSoloGame.rollHere(3);
        assertEquals(21, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first three frame");
    }

    @Test
    public void testSoloGameCanGetFinalStandingScoreWithSpares() {
        ISoloGame testSoloGame = this.getTestClassObject();

        testSoloGame.rollHere(4);
        testSoloGame.rollHere(6);
        //Gets a spare estimation
        assertEquals(10, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first frame (with spare)");

        testSoloGame.rollHere(5);
        testSoloGame.rollHere(5);
        //can get finalised score from first and estimation from second frame
        assertEquals(25, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first two frame (with spares)");

        testSoloGame.rollHere(2);
        testSoloGame.rollHere(3);
        //all 3 frames are finalised
        assertEquals(32, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first three frame (with spares)");
    }

    @Test
    public void testSoloGameCanGetFinalStandingScoreWithStrikes() {
        ISoloGame testSoloGame = this.getTestClassObject();

        testSoloGame.rollHere(10);
        //Strike estimation
        assertEquals(10, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first frame (with strikes)");

        testSoloGame.rollHere(10);
        //Estimation from both frames
        assertEquals(30, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first two frame (with strikes)");

        testSoloGame.rollHere(2);
        testSoloGame.rollHere(3);
        //all 3 frames have finalised scores
        assertEquals(42, testSoloGame.getRunningTotal(), "Should be able to calculate running total from first three frame (with strikes)");
        //25 + lisad(12 +5)
    }

    @Test
    public void testFrameCanCalculateFinalScoreForPerfectGame() {
        ISoloGame testSoloGame = this.getTestClassObject();
        for(int i = 0; i < 12; i++) {
            testSoloGame.rollHere(10);
        }
        assertEquals(300, testSoloGame.getRunningTotal(), "Should be able to calculate a perfect score game");
    }

    @Test
    public void testFrameWillReturnPlayerCurrentStateDescription() {
        ISoloGame testSoloGame = this.getTestClassObject();
        assertEquals(true, testSoloGame.getCurrentStateDescription() != null, "Current state description element should not be null");
    }

}