package prants.bowlingScore.soloGame;

import org.junit.jupiter.api.Test;
import prants.bowlingScore.player.IPlayer;
import prants.bowlingScore.player.Player;

import static org.junit.jupiter.api.Assertions.*;

class PlayerGameCurrentStateTest {

    private ISoloGame getTestClassObject(IPlayer player) {
        return new SoloGame(player);
    }

    private IPlayer getTestPlayer() {
        return new Player();
    }

    private IPlayerGameCurrentState getTestPlayerGameCurrentState(ISoloGame soloGame) {
        return new PlayerGameCurrentState(soloGame);
    }

    @Test
    public void testGameStateCanStorePlayerName() {
        IPlayer testPlayer = getTestPlayer();
        testPlayer.addNickname("testPlayer");
        ISoloGame soloGame = getTestClassObject(testPlayer);
        IPlayerGameCurrentState testState = getTestPlayerGameCurrentState(soloGame);
        assertEquals("testPlayer", testState.getPlayerNickname(), "GameCurrentState object should store players name");
    }

    @Test
    public void testGameStateCanStoreCurrentFrame() {
        IPlayer testPlayer = getTestPlayer();
        testPlayer.addNickname("testPlayer");
        ISoloGame soloGame = getTestClassObject(testPlayer);
        int firstFrameNr = soloGame.getCurrentFrameNrBowlingRuleSet();
        IPlayerGameCurrentState testState1 = getTestPlayerGameCurrentState(soloGame);

        soloGame.rollHere(10);
        soloGame.rollHere(10);
        int secondFrameNr = soloGame.getCurrentFrameNrBowlingRuleSet();
        IPlayerGameCurrentState testState2 = getTestPlayerGameCurrentState(soloGame);

        assertEquals(firstFrameNr, testState1.getCurrentFrameNr(), "Saved game state current frame nr needs to match the solo games one at that time");
        assertEquals(secondFrameNr, testState2.getCurrentFrameNr(), "Saved game state current frame nr needs to match the solo games one at that time");
    }

    @Test
    public void testGameStateCanStoreRunningScore() {
        IPlayer testPlayer = getTestPlayer();
        testPlayer.addNickname("testPlayer");
        ISoloGame soloGame = getTestClassObject(testPlayer);
        soloGame.rollHere(3);
        soloGame.rollHere(2);
        IPlayerGameCurrentState testState = getTestPlayerGameCurrentState(soloGame);
        assertEquals(soloGame.getRunningTotal(), testState.getCurrentRunningTotalScore(),
                "Saved game running total needs to match the solo game one");
    }

    @Test
    public void testGameStateStoresCorrectAmountOfFrames() {
        IPlayer testPlayer = getTestPlayer();
        testPlayer.addNickname("testPlayer");
        ISoloGame soloGame = getTestClassObject(testPlayer);
        IPlayerGameCurrentState testState = getTestPlayerGameCurrentState(soloGame);
        assertEquals(soloGame.getFrameList().size(), testState.getFrameStates().size(),
                "Saved game needs to have same amount of frames are base object");
    }

    @Test
    public void testGameStateRecognisesFinishedGame() {
        IPlayer testPlayer = getTestPlayer();
        testPlayer.addNickname("testPlayer");
        ISoloGame soloGame = getTestClassObject(testPlayer);
        assertEquals(false, getTestPlayerGameCurrentState(soloGame).isFinishedPlaying(),
                "New game should not be finished before any rolls");
        for(int i = 0; i < 12; i++) {
            soloGame.rollHere(10);
        }
        assertEquals(true, getTestPlayerGameCurrentState(soloGame).isFinishedPlaying(),
                "Game should be marked as finished after all rolls are done");
    }



}