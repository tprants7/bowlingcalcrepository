package prants.bowlingScore.game;

import prants.bowlingScore.player.IPlayer;
import prants.bowlingScore.player.Player;
import prants.bowlingScore.soloGame.IPlayerGameCurrentState;
import prants.bowlingScore.soloGame.ISoloGame;
import prants.bowlingScore.soloGame.SoloGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for all game classes, implements IGame interface
 * contains all common code for different game classes
 */
public abstract class AbstractGame implements IGame {
    protected List<ISoloGame> soloGames;

    /**
     * Constructor, sets up list that will store solo games for each added player
     */
    public AbstractGame() {
        soloGames = new ArrayList<>();
    }

    /**
     * Protected method for making a new player
     * @param nickname nickname designated for the new player
     * @return new player object that implements IPlayer interface
     */
    protected IPlayer makeNewPlayerWithNickname(String nickname) {
        IPlayer newPlayer = getNewPlayerObject();
        newPlayer.addNickname(nickname);
        return newPlayer;
    }

    /**
     * Protected abstract method for designating what class of player objects will be used
     * @return new custom class player object that implements IPlayer interface
     */
    protected abstract IPlayer getNewPlayerObject();

    /**
     * Protected method for initialising new solo game object
     * @param newPlayer IPlayer type object who will be injected as the new solo games player
     * @return solo game object that implements ISoloGame interface
     */
    protected ISoloGame makeNewSoloGameForPlayer(IPlayer newPlayer) {
        return getNewSoloGameObject(newPlayer);
    }

    /**
     * Protected abstract method for deciding what type of class is used for representing solo games
     * @param newPlayer IPlayer object who will be the designated player in the new solo game object
     * @return new object that implements ISoloGame interface
     */
    protected abstract ISoloGame getNewSoloGameObject(IPlayer newPlayer);

    /**
     * Protected script for reacting to a new added player, makes a solo game for that player
     * and stores that new game in a list
     * @param name String version of the new players nickname
     * @return boolean, true if a new player was added, false if the player was rejected
     */
    protected boolean newPlayerAddingScript(String name) {
        if(!checkIfAvailableNickName(name)) {
            return false;
        }
        soloGames.add(makeNewSoloGameForPlayer(makeNewPlayerWithNickname(name)));
        return true;
    }

    /**
     * Protected method for checking if a input nickname is acceptable as a new player
     * @param nickname String version of the nickname that is checked
     * @return boolean, true if this nickname can be used for a new player, false if not
     */
    protected boolean checkIfAvailableNickName(String nickname) {
        for(ISoloGame oneGame : this.soloGames) {
            if(oneGame.getPlayer().hasNickname(nickname)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addPlayer(String name) {
        return newPlayerAddingScript(name);
    }

    @Override
    public int getRunningScoreForPlayer(int id) {
        this.exceptionCheckForPlayerId(id);
        return this.soloGames.get(id).getRunningTotal();
    }

    @Override
    public int getRunningScoreForPlayer(String nickname) {
        int playerId = this.getPlayerId(nickname);
        return this.getRunningScoreForPlayer(playerId);
    }

    @Override
    public String getPlayerNickname(int id) {
        this.exceptionCheckForPlayerId(id);
        return this.soloGames.get(id).getPlayer().getMainNickname();
    }

    /**
     * Protected script that will check if an id can be accessed, if not will throw an exception
     * @param id id number that will be checked
     * @exception NullPointerException will be thrown if the id is not used
     */
    protected void exceptionCheckForPlayerId(int id) {
        if(this.soloGames.size() <= id) {
            throw new NullPointerException("There is no player with that id");
        }
        if(id < 0) {
            throw new NullPointerException("There is no player with that id");
        }
    }

    @Override
    public int getPlayerId(String nickname) {
        ISoloGame foundGame = findGameWithPlayerNickname(nickname);
        if(foundGame == null) {
            throw new NullPointerException("There is no player with that nickname");
        }
        else {
            return this.soloGames.indexOf(foundGame);
        }
    }

    /**
     * Protected method for looking up a soloGame with the nickname of the player who is plays in it
     * @param nickname Nickname of the player who's solo game is searched for
     * @return first instance of ISoloGame that has a player with input nickname, null if there is no such game
     */
    protected ISoloGame findGameWithPlayerNickname(String nickname) {
        for(ISoloGame oneSoloGame : this.soloGames) {
            if(oneSoloGame.getPlayer().hasNickname(nickname)) {
                return oneSoloGame;
            }
        }
        return null;
    }

    @Override
    public boolean canRollForPlayer(String nickname) {
        int playerId = this.getPlayerId(nickname);
        return this.canRollForPlayer(playerId);
    }

    @Override
    public boolean canRollForPlayer(int id) {
        this.exceptionCheckForPlayerId(id);
        return this.soloGames.get(id).needsRolls();
    }

    @Override
    public boolean addPointsForPlayer(String nickname, int points) {
        int playerId = this.getPlayerId(nickname);
        return this.addPointsForPlayer(playerId, points);
    }

    @Override
    public boolean addPointsForPlayer(int id, int points) {
        if(!canRollForPlayer(id)) {
            return false;
        }
        this.soloGames.get(id).rollHere(points);
        return true;
    }

    @Override
    public int getPlayerCurrentFrame(String nickname) {
        int playerId = this.getPlayerId(nickname);
        return getPlayerCurrentFrame(playerId);
    }

    @Override
    public int getPlayerCurrentFrame(int id) {
        this.exceptionCheckForPlayerId(id);
        return this.soloGames.get(id).getCurrentFrameNrBowlingRuleSet();
    }

    @Override
    public List<String> getNamesOfAllThePlayers() {
        return this.makeAListOfPlayerNames(this.extractAllPlayers());
    }

    /**
     * Makes a list of players names from a list of players
     * @param players list of players
     * @return list of the names of those players
     */
    protected List<String> makeAListOfPlayerNames(List<IPlayer> players) {
        List<String> nameList = new ArrayList<>();
        for(IPlayer onePlayer : players) {
            nameList.add(onePlayer.getMainNickname());
        }
        return nameList;
    }

    /**
     * Makes a list of all players currently in the game
     * @return
     */
    protected List<IPlayer> extractAllPlayers() {
        List<IPlayer> playerList = new ArrayList<>();
        for(ISoloGame oneGame : this.soloGames) {
            playerList.add(oneGame.getPlayer());
        }
        return playerList;
    }

    @Override
    public int getCurrentlyStandingPinsForPlayer(int id) {
        if(canRollForPlayer(id)) {
            return this.soloGames.get(id).getStandingPinsInCurrentFrame();
        }
        return 0;
    }

    @Override
    public IPlayerGameCurrentState getCurrentStateDescriptionForPlayer(int id) {
        exceptionCheckForPlayerId(id);
        return this.soloGames.get(id).getCurrentStateDescription();
    }

    @Override
    public List<IPlayerGameCurrentState> getAllPlayersCurrentStates() {
        List<IPlayerGameCurrentState> finalList = new ArrayList<>();
        for(ISoloGame oneSoloGame : this.soloGames) {
            finalList.add(oneSoloGame.getCurrentStateDescription());
        }
        return finalList;
    }


}
