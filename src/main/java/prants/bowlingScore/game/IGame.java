package prants.bowlingScore.game;

import prants.bowlingScore.player.IPlayer;
import prants.bowlingScore.soloGame.IPlayerGameCurrentState;

import java.util.List;

/**
 * Interface class for game objects, that represent one game of bowling with multiple players
 */
public interface IGame {

    /**
     * For adding a player to the game
     * @param name nickname that will be used for the player
     * @return boolean, true if the player was added, false if it didn't (duplicate name)
     */
    public boolean addPlayer(String name);

    /**
     * For getting the running total score for a player
     * @param id identification number of the player who's score you want, it matches the location number in storage list.
     * @return int value of the current running score across all frames
     */
    public int getRunningScoreForPlayer(int id);

    /**
     * For getting the running total score for a player
     * @param nickname designated nickname of the player who's score you want.
     * @return int value of the current running score across all frames
     */
    public int getRunningScoreForPlayer(String nickname);

    /**
     * For getting the designated nickname of a certain player by their id number (list location nr)
     * @param id identification number of the player (list location nr)
     * @return String value of the nickname
     */
    public String getPlayerNickname(int id);

    /**
     * For getting the id (list location nr) of a player by their nickname.
     * @param nickname nickname of the player who's id is returned
     * @return int value of the id (list location nr)
     */
    public int getPlayerId(String nickname);

    /**
     * Checks if a player can add new rolls
     * @param nickname nickname of the player in question
     * @return boolean, true if they can roll, false if they can't
     */
    public boolean canRollForPlayer(String nickname);

    /**
     * Checks if a player can add new rolls
     * @param id identification number of the player (list location nr)
     * @return boolean, true if they can roll, false if they can't
     */
    public boolean canRollForPlayer(int id);

    /**
     * For adding new roll score for a player
     * @param nickname nickname of the player who you want to add points to
     * @param points amount of pins they knocked over with the roll
     * @return boolean, true if this score got added, false if it got rejected
     */
    public boolean addPointsForPlayer(String nickname, int points);

    /**
     * For adding new roll score for a player
     * @param id identification number of the player (list location nr)
     * @param points amount of pins they knocked over with the roll
     * @return boolean, true if this score got added, false if it got rejected
     */
    public boolean addPointsForPlayer(int id, int points);

    /**
     * Getter for what frame a player is currently on, this uses Bowling rule set numbering,
     * first frame is 1 and goes up to 10
     * @param nickname nickname of the player
     * @return int, frame number, starts with 1
     */
    public int getPlayerCurrentFrame(String nickname);

    /**
     * Getter for what frame a player is currently on, this uses Bowling rule set numbering,
     * first frame is 1 and goes up to 10
     * @param id identification number of the player (list location nr)
     * @return int, frame number, starts with 1
     */
    public int getPlayerCurrentFrame(int id);

    /**
     * Getter for a list of all player names in the game
     * @return List of all the names of players taking part
     */
    public List<String> getNamesOfAllThePlayers();

    /**
     * Getter that retrieves how many pins are standing for a specific player in their current frame
     * @param id player id who is asked about
     * @return int value of how many pins are standing
     */
    public int getCurrentlyStandingPinsForPlayer(int id);

    /**
     * For getting a player state description for a specific player
     * @param id id of the player asked about
     * @return  IPlayerGameCurrentState type object that contains data about input players game
     */
    public IPlayerGameCurrentState getCurrentStateDescriptionForPlayer(int id);

    /**
     * For getting a list off all player current game states
     * @return List of game states, list is empty if there are no players
     */
    public List<IPlayerGameCurrentState> getAllPlayersCurrentStates();
}
