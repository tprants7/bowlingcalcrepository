package prants.bowlingScore.game;

import prants.bowlingScore.player.IPlayer;
import prants.bowlingScore.player.Player;
import prants.bowlingScore.soloGame.ISoloGame;
import prants.bowlingScore.soloGame.SoloGame;

/**
 * Standard game class, extends AbstractGame
 * represents one game of bowling with multiple players
 */
public class Game extends AbstractGame {

    /**
     * constructor
     */
    public Game() {
        super();
    }

    @Override
    protected IPlayer getNewPlayerObject() {
        return new Player();
    }

    @Override
    protected ISoloGame getNewSoloGameObject(IPlayer newPlayer) {
        return new SoloGame(newPlayer);
    }

}
