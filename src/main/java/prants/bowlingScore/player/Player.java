package prants.bowlingScore.player;

/**
 * Standard class for players, extends AbstractPlayer
 * represents one player in a game of bowling
 */
public class Player extends AbstractPlayer {

    /**
     * Constructor
     */
    public Player() {
        super();
    }
}
