package prants.bowlingScore.player;

/**
 * Interface for player classes, that represent a player in a bowling game
 */
public interface IPlayer {

    /**
     * Checks if player posses the input nickname
     * @param nickname nickname that is being checked
     * @return boolean, true if this player has the nickname, false if not
     */
    public boolean hasNickname(String nickname);

    /**
     * For adding a new nickname to the player
     * @param nickname new nickname getting added
     * @return boolean, true if the nickname was added, false if it was rejected
     */
    public boolean addNickname(String nickname);

    /**
     * Getter for the players main nickname, main nickname is the first saved nickname
     * @return String value of the saved nickname
     */
    public String getMainNickname();
}
