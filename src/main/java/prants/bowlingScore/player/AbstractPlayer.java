package prants.bowlingScore.player;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for all player classes, implements IPlayer interface,
 * contains all common code between all the player classes
 */
public abstract class AbstractPlayer implements IPlayer {
    protected List<String> nicknames;

    /**
     * Constructor, sets up list for player nicknames
     */
    public AbstractPlayer() {
        this.nicknames = new ArrayList<>();
    }

    @Override
    public boolean hasNickname(String nickname) {
        return this.nicknames.contains(nickname);
    }

    @Override
    public boolean addNickname(String nickname) {
        if(!this.nicknames.contains(nickname)) {
            this.nicknames.add(nickname);
            return true;
        }
        return false;
    }

    @Override
    public String getMainNickname() {
        if(this.nicknames.isEmpty()) {
            return null;
        }
        return this.nicknames.get(0);
    }
}
