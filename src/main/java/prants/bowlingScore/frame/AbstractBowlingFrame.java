package prants.bowlingScore.frame;

import prants.bowlingScore.result.IResult;
import prants.bowlingScore.soloGame.ISoloGame;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBowlingFrame implements IBowlingFrame {
    protected List<IResult> resultList;
    protected ISoloGame masterGame;

    /**
     * Constructor
     */
    public AbstractBowlingFrame() {
        this.resultList = new ArrayList<>();
    }

    @Override
    public boolean canRollHere() {
        return this.rollCheckCalculation();
    }

    /**
     * Method that checks if this frame allows for more rolls
     * @return true if more rolls are allowed, false if not
     */
    protected  abstract boolean rollCheckCalculation();

    @Override
    public boolean isScoreFinalised() {
        return scoreFinaliseCheck();
    }

    /**
     * Checks if there is enough data to get the final score of this frame
     * @return true if there is enough data, false if more rolls are needed in next frames
     */
    protected abstract boolean scoreFinaliseCheck();

    /**
     * Checks if frame final score can be calculated in case there was a strike made here
     * @return true if it can be, false if more rolls are needed or there isn't access to other frames
     */
    protected boolean finishCheckForStrike() {
        if(!this.hasSoloGameDependency()) {
            return false;
        }
        return this.hasNrOfNextRollsDone(2);
    }

    /**
     * Checks if frame final score can be calculated in case there was a spare made here
     * @return true if it can be, false if more rolls are needed or there isn't access to other frames
     */
    protected boolean finishCheckForSpare() {
        if(!this.hasSoloGameDependency()) {
            return false;
        }
        return this.hasNrOfNextRollsDone(1);
    }

    @Override
    public boolean addResult(int result) {
        if(!canRollHere()) {
            return false;
        }
        return this.addingNewResultForNormal(result);
    }

    /**
     * method that adds a new roll for a standard frame (not designated as final tenth one)
     * @param result int value of knocked down pins
     * @return true if the roll was accepted, false if not
     */
    protected boolean addingNewResultForNormal(int result) {
        this.resultList.add(makeResultObject(result, this.findOutPreviousPins()));
        return true;
    }

    /**
     * Method for making a specific IResult object, the class is assigned in extended classes
     * @param result int value for how many pins were knocked down
     * @param previousPins int value for how many pins were standing before the throw was done
     * @return New result object that extends IResult
     */
    protected abstract IResult makeResultObject(int result, int previousPins);

    /**
     * Method for calculating the amount of pins standing for the next throw
     * @return int value for how many pins are standing atm
     */
    protected  abstract int findOutPreviousPins();

    @Override
    public int getScore() {
        if(!isScoreFinalised()) {
            //throw new NullPointerException("Cant calculate score if the frame is not finished");
            return this.scoreEstimationCalculations();
        }
        if(isStrike()) {
            return this.calculateStrikeScore();
        }
        if(isSpare()) {
            return this.calculateSpareScore();
        }
        return this.calculateLessThen10Score();
    }

    /**
     * Method for calculating the final score for a strike
     * @return int value for the final total score of the frame, if there was a strike
     */
    protected  abstract int calculateStrikeScore();

    /**
     * Method for calculating the final score for a spare
     * @return int value for the final total score of the frame, if there was a spare
     */
    protected abstract int calculateSpareScore();

    /**
     * method for calculating the final score in case there wasn't a strike or a spare
     * @return int value for the final total score of the frame
     */
    protected int calculateLessThen10Score() {
        int finalScore = 0;
        for(IResult oneResult : this.resultList) {
            finalScore = finalScore + oneResult.getKnockedDownPins();
        }
        return finalScore;
    }

    @Override
    public boolean isFinalFrame() {
        return isThisLastFrame();
    }

    /**
     * Abstract method for marking down if a inheriting class is supposed to be used as a last frame in the game
     * @return boolean, true if its supposed to be last frame, false if not
     */
    protected abstract boolean isThisLastFrame();

    @Override
    public boolean isStrike() {
        if(this.resultList.isEmpty()) {
            return false;
        }
        if(this.resultList.get(0).getRemainingPins() > 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isSpare() {
        if(this.isStrike()) {
            return false;
        }
        if(this.resultList.size() < 2) {
            return false;
        }
        if(this.resultList.get(1).getRemainingPins() > 0) {
            return false;
        }
        return true;
    }

    /**
     * Check function that checks that this is a frame with a less then 10 total score
     * @return true if this is a not a strike or spare frame, false if not
     */
    protected boolean isBelow10Result() {
        if(this.isStrike()) {
            return false;
        }
        if(this.isSpare()) {
            return false;
        }
        if(this.rollCheckCalculation()) {
            return false;
        }
        return true;
    }

    @Override
    public boolean hasSoloGameDependency() {
        return this.masterGame != null;
    }

    @Override
    public void addSoloGameDependency(ISoloGame soloGame) {
        this.masterGame = soloGame;
    }

    /**
     * Checks if there are an input amount of rolls done in the next two frames
     * @param nr nr of rolls we are looking for
     * @return if there are at least as many rolls done as the input nr
     */
    protected boolean hasNrOfNextRollsDone(int nr) {
        int amountOfRollsFound = 0;
        IBowlingFrame nextFrame = this.getNextFrame(this);
        amountOfRollsFound = nextFrame.getAmountOfRolls();
        if(!nextFrame.isFinalFrame()) {
            IBowlingFrame nextNextFrame = this.getNextFrame(nextFrame);
            amountOfRollsFound = amountOfRollsFound + nextNextFrame.getAmountOfRolls();
        }
        return amountOfRollsFound >= nr;
    }

    /**
     * Estimation calculation that will calculate a summarizes score of all pins knocked down in this frame and up to
     * one roll worth from next frame
     * @return int value for the estimated score
     */
    protected int scoreEstimationCalculations() {
        int totalScore = this.calculateLessThen10Score();
        if(!this.isFinalFrame()) {
            if(this.hasSoloGameDependency()) {
                if(!this.getNextFrame(this).getAllResults().isEmpty()) {
                    totalScore = totalScore +this.getNextFrame(this).getAllResults().get(0).getKnockedDownPins();
                }
            }
        }
        return totalScore;
    }

    /**
     * Method for getting a sum value of rolls from next frames
     * @param nr how many next rolls will be summarized
     * @return int sum value
     */
    protected int summarizeNrOfNextRolls(int nr) {
        int counter = nr;
        int result = 0;
        IBowlingFrame frameBookMark = this;
        while(counter > 0) {
            frameBookMark = this.getNextFrame(frameBookMark);
            result = result + summarizeNrOfNextRollsInAFrame(counter, frameBookMark);
            counter = counter - frameBookMark.getAmountOfRolls();
        }
        return result;
    }

    /**
     * Method for summarizing up to a set amount of rolls in a specific next frame
     * @param nr countdown number for how many rolls should be summarized
     * @param frame frame element from where the rolls will be taken from
     * @return int sum value of knocked down pins in found rolls
     */
    protected int summarizeNrOfNextRollsInAFrame(int nr, IBowlingFrame frame) {
        int result = 0;
        for(IResult oneResult : frame.getAllResults()) {
            if(nr > 0) {
                result = result + oneResult.getKnockedDownPins();
                nr--;
            }
        }
        return result;
    }

    @Override
    public List<IResult> getAllResults() {
        return new ArrayList<>(this.resultList);
    }

    /**
     * For getting a next frame, only works if this frame has had a ISoloGame element assigned to it
     * @param fromThisFrame Frame from which you wanna go next to
     * @return IBowlingFrame element that is next of the input frame
     */
    protected IBowlingFrame getNextFrame(IBowlingFrame fromThisFrame) {
        int newFrameId = this.masterGame.getFrameList().indexOf(fromThisFrame) + 1;
        return this.masterGame.getFrameList().get(newFrameId);
    }

    @Override
    public int getAmountOfRolls() {
        return this.resultList.size();
    }

    @Override
    public int getCurrentlyStandingPins() {
        return this.findOutPreviousPins();
    }
}
