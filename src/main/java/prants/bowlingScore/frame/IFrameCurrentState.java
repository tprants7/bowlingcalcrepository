package prants.bowlingScore.frame;

import java.util.List;

public interface IFrameCurrentState {

    /**
     * Getter for frames current running score
     * @return int value for currently finalised score
     */
    public int getCurrentFrameScore();

    /**
     * Getter for a list of the knocked down pins of rolls,
     * list size matches how many rolls have been done in the frame
     * @return List of integers that stand for knocked down pins with each throw
     */
    public List<Integer> getRollValues();

    /**
     * Getter for the frames assigned number in the bowling game, from 1 to 10
     * @return int, frames assigned number value
     */
    public int getFrameNr();
}
