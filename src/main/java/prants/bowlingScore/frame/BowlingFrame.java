package prants.bowlingScore.frame;

import prants.bowlingScore.result.IResult;
import prants.bowlingScore.result.Result;

public class BowlingFrame extends AbstractBowlingFrame {

    /**
     * Constructor
     */
    public BowlingFrame() {
        super();
    }

    @Override
    protected IResult makeResultObject(int result, int previousPins) {
        return new Result(result, previousPins);
    }

    @Override
    protected boolean rollCheckCalculation() {
        if(this.findOutPreviousPins() < 1) {
            return false;
        }
        if(this.resultList.size() > 1) {
            return false;
        }
        return true;
    }

    @Override
    protected boolean scoreFinaliseCheck() {
        if(this.isStrike()) {
            return this.finishCheckForStrike();
        }
        if(this.isSpare()) {
            return this.finishCheckForSpare();
        }
        if(isBelow10Result()){
            return true;
        }
        return false;
    }

    @Override
    protected int findOutPreviousPins() {
        if(this.resultList.isEmpty()) {
            return 10;
        }
        else {
            return resultList.get(resultList.size()-1).getRemainingPins();
        }
    }

    @Override
    protected  int calculateStrikeScore() {
        return summariseScoreForStrike();
    }

    /**
     * Method that summarizes the score for a strike, by combining frame score with extra points
     * @return total strike score
     */
    protected int summariseScoreForStrike() {
        return calculateLessThen10Score() + this.summarizeNrOfNextRolls(2);
    }

    @Override
    protected int calculateSpareScore() {
        return summariseScoreForSpare();
    }

    /**
     * Method that summarizes the score for a spare, by combining frame score with extra points
     * @return total spare score
     */
    protected int summariseScoreForSpare() {
        return calculateLessThen10Score() + this.summarizeNrOfNextRolls(1);
    }

    @Override
    protected boolean isThisLastFrame() {
        return false;
    }

}
