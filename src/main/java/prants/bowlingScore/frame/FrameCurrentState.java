package prants.bowlingScore.frame;

import prants.bowlingScore.result.IResult;

import java.util.ArrayList;
import java.util.List;

public class FrameCurrentState implements IFrameCurrentState {
    private int currentScore;
    private int frameNr;
    private List<Integer> rolls;

    /**
     * Constructor
     * @param source bowling game frame, its value will be extracted and stored in this object
     * @param frameNr Nr for this frame, should follow bowling game rules
     */
    public FrameCurrentState(IBowlingFrame source, int frameNr) {
        this.assignCurrentScore(source);
        this.rolls = convertRollsToList(source.getAllResults());
        this.frameNr = frameNr;
    }

    /**
     * For saving current game score, value will be 0 if actual result is not yet finalised
     * @param source frame where the data is collected
     */
    private void assignCurrentScore(IBowlingFrame source) {
        if(source.isScoreFinalised()) {
            this.currentScore = source.getScore();
        }
        else {
            this.currentScore = 0;
        }
    }

    /**
     * For turning rolls into a list of integers that stores the amount of pins knocked down in those rolls
     * @param results list of IResult elements that are the active roll calculating elements
     * @return List of integers that were made based on the input
     */
    private List<Integer> convertRollsToList(List<IResult> results) {
        List<Integer> finalList = new ArrayList<>();
        for(IResult oneResult : results) {
            finalList.add(oneResult.getKnockedDownPins());
        }
        return finalList;
    }

    @Override
    public int getCurrentFrameScore() {
        return this.currentScore;
    }

    @Override
    public List<Integer> getRollValues() {
        return new ArrayList<Integer>(this.rolls);
    }

    @Override
    public int getFrameNr() {
        return this.frameNr;
    }
}
