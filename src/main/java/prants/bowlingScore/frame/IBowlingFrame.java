package prants.bowlingScore.frame;

import prants.bowlingScore.result.IResult;
import prants.bowlingScore.soloGame.ISoloGame;

import java.util.List;

/**
 * Interface for bowlingFrame classes that represent one frame of a bowling game for one player
 */
public interface IBowlingFrame {

    /**
     * Method that checks if this frame allows new rolls
     * @return boolean, true if this frame allows rolls, false if not
     */
    public boolean canRollHere();

    /**
     * Method that checks if this frames score is finalised, as strikes and spares require a nr of rolls in
     * other frames to finalise
     * @return boolean, true if this frames score is finalised, false if not
     */
    public boolean isScoreFinalised();

    /**
     * For adding a new roll to this frame
     * @param result int value between 0-10 for how many pins were knocked down with one roll
     * @return boolean, true if this result was added to the frame, false if it was not added
     */
    public boolean addResult(int result);

    /**
     * For getting current score value of the frame, if the score is not finalised,
     * will give an estimation (summarises as many rolls as there are)
     * @return int value of gained points in this frame
     */
    public int getScore();

    /**
     * Checks the value of final frame flag of this frame
     * @return boolean value of the flag
     */
    public boolean isFinalFrame();

    /**
     * Checks if the result of the entire frame is strike
     * @return boolean, true if strike, false if not
     */
    public boolean isStrike();

    /**
     * Checks if the result of the entire frame is spare
     * @return boolean, true if spare, false if not
     */
    public boolean isSpare();

    /**
     * Checks if this frame has a SoloGame set as a master, its used for calculating score
     * @return boolean, true if the dependency has been set, false if not
     */
    public boolean hasSoloGameDependency();

    /**
     * For adding a solo game dependency for the frame, its used for calculating score
     * @param soloGame ISoloGame object that will become the dependency
     */
    public void addSoloGameDependency(ISoloGame soloGame);

    /**
     * For getting the amount of rolls registered in this frame
     * @return int value for amount of rolls, can be between 1 and 3
     */
    public int getAmountOfRolls();

    /**
     * for getting all IResult type objects in this frame
     * @return Copy list of IResult type objects saved in this frame
     */
    public List<IResult> getAllResults();

    /**
     * For getting currently standing pins amount in this frame
     * @return int value of how many pins are standing for the next roll of this frame
     */
    public int getCurrentlyStandingPins();
}
