package prants.bowlingScore.frame;

import prants.bowlingScore.result.IResult;
import prants.bowlingScore.result.Result;

public class BowlingLastFrame extends AbstractBowlingFrame {

    @Override
    protected IResult makeResultObject(int result, int previousPins) {
        return new Result(result, previousPins);
    }

    @Override
    protected boolean rollCheckCalculation() {
        if(this.resultList.isEmpty()) {
            return true;
        }
        if(this.resultList.size() < 2) {
            return true;
        }
        if(this.isStrike() && this.resultList.size() < 3) {
            return true;
        }
        if(this.isSpare() && this.resultList.size() < 3) {
            return true;
        }
        return false;
    }

    @Override
    protected boolean scoreFinaliseCheck() {
        if(this.isStrike() || this.isSpare()) {
            return this.resultList.size() > 2;
        }
        if(this.resultList.size() > 1){
            return true;
        }
        return false;
    }

    @Override
    protected int findOutPreviousPins() {
        if(this.resultList.isEmpty()) {
            return 10;
        }
        if(resultList.get(resultList.size() -1).getRemainingPins() > 0) {
            return resultList.get(resultList.size() -1).getRemainingPins();
        }
        return 10;
    }

    @Override
    protected  int calculateStrikeScore() {
        return calculateStrikeOrSpareScore();
    }

    @Override
    protected int calculateSpareScore() {
        return calculateStrikeOrSpareScore();
    }

    /**
     * Method for calculating the final score for a spare or a strike on a designated final frame
     * @return int value for the final total score of the frame, if there was a spare or a strike
     */
    protected int calculateStrikeOrSpareScore() {
        int result = 0;
        for(IResult oneResult : this.resultList) {
            result = result + oneResult.getKnockedDownPins();
        }
        return result;
    }

    @Override
    protected boolean isThisLastFrame() {
        return true;
    }
}
