package prants.bowlingScore.soloGame;

import prants.bowlingScore.frame.FrameCurrentState;
import prants.bowlingScore.frame.IBowlingFrame;
import prants.bowlingScore.frame.IFrameCurrentState;

import java.util.ArrayList;
import java.util.List;

public class PlayerGameCurrentState implements IPlayerGameCurrentState {
    private String playerNickname;
    private int currentFrameNr;
    private int currentRunningTotalScore;
    private List<IFrameCurrentState> currentFrameStates;
    private boolean finished;

    /**
     * Constructor
     * @param soloGame ISoloGame that will have its state saved
     */
    public PlayerGameCurrentState(ISoloGame soloGame) {
        this.playerNickname = soloGame.getPlayer().getMainNickname();
        this.currentFrameNr = soloGame.getCurrentFrameNrBowlingRuleSet();
        this.currentRunningTotalScore = soloGame.getRunningTotal();
        this.currentFrameStates = makeListOfFrameStates(soloGame.getFrameList());
        this.finished = !soloGame.needsRolls();
    }

    /**
     * Makes a list of all individual frame states of saved solo game
     * @param bowlingFrames List of frames stored in the solo game
     * @return List of IFrameCurrentState elements based on input frames
     */
    private List<IFrameCurrentState> makeListOfFrameStates(List<IBowlingFrame> bowlingFrames) {
        List<IFrameCurrentState> finalList = new ArrayList<>();
        for(IBowlingFrame oneFrame : bowlingFrames) {
            finalList.add(new FrameCurrentState(oneFrame, bowlingFrames.indexOf(oneFrame)+1));
        }
        return finalList;
    }

    @Override
    public String getPlayerNickname() {
        return this.playerNickname;
    }

    @Override
    public int getCurrentFrameNr() {
        return this.currentFrameNr;
    }

    @Override
    public int getCurrentRunningTotalScore() {
        return this.currentRunningTotalScore;
    }

    @Override
    public List<IFrameCurrentState> getFrameStates() {
        return new ArrayList<>(this.currentFrameStates);
    }

    @Override
    public boolean isFinishedPlaying() {
        return this.finished;
    }
}
