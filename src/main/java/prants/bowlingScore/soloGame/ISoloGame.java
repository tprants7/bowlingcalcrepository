package prants.bowlingScore.soloGame;

import prants.bowlingScore.frame.IBowlingFrame;
import prants.bowlingScore.player.IPlayer;

import java.util.List;

/**
 * Interface for solo game classes, these represent one players part in a game of bowling
 */
public interface ISoloGame {

    /**
     * Checks if this solo game still needs new rolls to finish
     * @return boolean, true if more rolls are needed, false if the game is finished
     */
    public boolean needsRolls();

    /**
     * Getter for the current running total of this solo game
     * @return int value of the current score
     */
    public int getRunningTotal();

    /**
     * Getter for, what number frame the player is currently on
     * uses bowling rule set, starts at nr 1
     * @return int, frame number that player is currently on, starts at 1
     */
    public int getCurrentFrameNrBowlingRuleSet();

    /**
     * Getter for the current frame object that player is on
     * @return IBowlingFrame interface object, frame that player is currently rolling for
     */
    public IBowlingFrame getCurrentFrame();

    /**
     * Getter that returns a copy of the frame list stored in this solo game
     * @return List of IBowlingFrame objects, copy of the list stored in this solo game
     */
    public List<IBowlingFrame> getFrameList();

    /**
     * Method for adding a new roll score to this solo game
     * @param result int value of how many pins were knocked down with the roll
     * @return boolean, true if this result was successfully added, false if it got rejected
     */
    public boolean rollHere(int result);

    /**
     * Getter for the IPlayer type of player who plays in this solo game
     * @return player object saved in this solo game
     */
    public IPlayer getPlayer();

    /**
     * This allows access to currently active frames standing pin count
     * @return int value for how many pins are standing in the currently active frame
     */
    public int getStandingPinsInCurrentFrame();

    /**
     * This makes and returns current player game state description of this solo game
     * @return IPlayerGameCurrentState class object that represents all important current game value
     */
    public IPlayerGameCurrentState getCurrentStateDescription();


}
