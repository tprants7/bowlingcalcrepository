package prants.bowlingScore.soloGame;

import prants.bowlingScore.frame.BowlingFrame;
import prants.bowlingScore.frame.BowlingLastFrame;
import prants.bowlingScore.frame.IBowlingFrame;
import prants.bowlingScore.player.IPlayer;

/**
 * Class for standard soloGames, this class extends AbstractSoloGame and
 * represents one players activity in a game of bowling
 */
public class SoloGame extends AbstractSoloGame {

    /**
     * Constructor
     * @param player IPlayer version of a player who plays in this solo game
     */
    public SoloGame(IPlayer player) {
        super(player);
    }

    @Override
    protected int getStandardFrameAmount() {
        return 10;
    }

    @Override
    protected IBowlingFrame getNewFrame() {
        return new BowlingFrame();
    }

    @Override
    protected IPlayerGameCurrentState makeCurrentStateObject(ISoloGame soloGame) {
        return new PlayerGameCurrentState(soloGame);
    }

    @Override
    protected IBowlingFrame getNewLastFrameObject() {
        return new BowlingLastFrame();
    }
}
