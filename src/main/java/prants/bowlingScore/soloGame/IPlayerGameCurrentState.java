package prants.bowlingScore.soloGame;

import prants.bowlingScore.frame.IFrameCurrentState;

import java.util.List;

public interface IPlayerGameCurrentState {

    /**
     * Returns this game states player's name
     * @return String value of the name
     */
    public String getPlayerNickname();

    /**
     * Getter for the current active frame
     * @return active frames number, start from 1
     */
    public int getCurrentFrameNr();

    /**
     * Getter for current running total score
     * @return current total score of finalised frames
     */
    public int getCurrentRunningTotalScore();

    /**
     * Returns all IFrameCurrentStates of individual frames of this game
     * @return list of IFrameCurrentState elements stored in this game state
     */
    public List<IFrameCurrentState> getFrameStates();

    /**
     * Checks if this solo game has finished
     * @return true if the game is finished, false if it can still be rolled for
     */
    public boolean isFinishedPlaying();
}
