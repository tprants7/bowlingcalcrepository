package prants.bowlingScore.soloGame;

import prants.bowlingScore.frame.IBowlingFrame;
import prants.bowlingScore.player.IPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract solo game object that uses the interface and
 * posses all shared code between any solo game class
 */
public abstract class AbstractSoloGame implements ISoloGame {
    protected IPlayer player;
    protected List<IBowlingFrame> frames;
    protected IBowlingFrame currentFrame;

    /**
     * Constructor
     * @param player player who will play in this solo game
     */
    public AbstractSoloGame(IPlayer player) {
        this.player = player;
        this.makeFrames();
        this.setFirstFrameAsCurrent();
    }

    /**
     * Protected method that sets up local storage for frames and
     * also creates all 10 frames that makes up a game of bowling
     */
    protected void makeFrames() {
        this.frames = new ArrayList<>();
        this.makeStandardFrames();
        this.makeFinalFrame();
    }

    /**
     * Protected method for making 9 of the frames used in bowling,
     * that all share the same function logic
     */
    protected void makeStandardFrames() {
        for(int i = 0; i < this.getStandardFrameAmount() -1; i++) {
            this.frames.add(getNewFrame());
            this.frames.get(i).addSoloGameDependency(this);
        }
    }

    /**
     * Protected method for making the last frame used in bowling,
     * it has different rules compared to the 9 other frames
     */
    protected void makeFinalFrame() {
        this.frames.add(this.getNewLastFrameObject());
        this.frames.get(getFinalFrameNr()).addSoloGameDependency(this);
    }

    /**
     * Abstract method for getting the class realization for the last frame
     * @return new frame object that will represent the last frame in this solo game
     */
    protected abstract IBowlingFrame getNewLastFrameObject();

    /**
     * Protected method for designating first frame as the the currently used frame,
     * this is meant to be used only on creation of a solo game object
     */
    protected void setFirstFrameAsCurrent() {
        this.currentFrame = this.frames.get(0);
    }

    /**
     * Protected method that will push the book mark for current frame ahead by one frame,
     * this is meant to be called on after finishing all needed rolls in the current active frame
     */
    protected void moveToNextFrame() {
        if(this.currentFrame.isFinalFrame()) {
            return;
        }
        int nextFrameNr = this.frames.indexOf(this.currentFrame) +1;
        this.currentFrame = this.frames.get(nextFrameNr);
    }

    /**
     * Abstract protected method that gives the standard frame amount in a bowling game
     * @return int value for how many frames should be in the game (Bowling rule set requires this to be 10)
     */
    protected abstract int getStandardFrameAmount();

    /**
     * Abstract protected method for getting a new object that implements IBowlingFrame interface,
     * this is the place where active bowling frame class can be changed
     * @return a new object that implements IBowlingFrame interface
     */
    protected abstract IBowlingFrame getNewFrame();

    @Override
    public boolean needsRolls() {
        return this.frames.get(getFinalFrameNr()).canRollHere();
    }

    @Override
    public boolean rollHere(int result) {
        boolean successFlag = this.currentFrame.addResult(result);
        if(!this.currentFrame.canRollHere()) {
            this.moveToNextFrame();
        }
        return successFlag;
    }

    @Override
    public int getRunningTotal() {
        return summarizeFinalsScoresAcrossAllFrames();
    }

    /**
     * Script for summarising final score across all frames
     * @return int summarization of all finalised scores across the solo game
     */
    protected int summarizeFinalsScoresAcrossAllFrames() {
        int result = 0;
        for(IBowlingFrame oneFrame : this.frames) {
            result = result + getRunningScoreFromOneFrame(oneFrame);
        }
        return result;
    }

    /**
     * Script for getting running score from one frame of bowling solo game
     * @param oneFrame Frame from where the score is retrieved from
     * @return int value for the score, its 0 if the frame does not have a finalised score
     */
    protected int getRunningScoreFromOneFrame(IBowlingFrame oneFrame) {
        /*if(oneFrame.isScoreFinalised()) {
            return oneFrame.getScore();
        }
        else {
            return 0;
        }*/
        return oneFrame.getScore();
    }

    @Override
    public int getCurrentFrameNrBowlingRuleSet() {
        return this.frames.indexOf(this.currentFrame) + 1;
    }

    @Override
    public IBowlingFrame getCurrentFrame() {
        return this.currentFrame;
    }

    @Override
    public List<IBowlingFrame> getFrameList() {
        return new ArrayList<>(this.frames);
    }

    /**
     * Protected getter for final frame number, will dynamically adjust to currently made frame list
     * @return int value for the location number of the final frame
     */
    protected int getFinalFrameNr() {
        return this.frames.size() -1;
    }

    @Override
    public IPlayer getPlayer() {
        return this.player;
    }

    @Override
    public int getStandingPinsInCurrentFrame() {
        return this.currentFrame.getCurrentlyStandingPins();
    }

    @Override
    public IPlayerGameCurrentState getCurrentStateDescription() {
        return this.makeCurrentStateObject(this);
    }

    /**
     * Method that actually makes a current game state object that implements IPlayerGameCurrentState
     * @param soloGame ISoloGame that will have its current state saved
     * @return object of IPlayerGameCurrentState that saved the input data
     */
    protected abstract IPlayerGameCurrentState makeCurrentStateObject(ISoloGame soloGame);

}
