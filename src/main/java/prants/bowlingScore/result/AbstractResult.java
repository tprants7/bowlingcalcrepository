package prants.bowlingScore.result;

/**
 * Abstract class for all result classes, implements IResult interface,
 * contains common code for any result classes
 */
public abstract class AbstractResult implements IResult {
    protected int startPins;
    protected int knockedDownPins;

    /**
     * Constructor
     * @param knockedDownPins int value for how many pins were knocked down with this roll (between 0 and 10)
     * @param startNrOfPins int value for how many pins were standing before the roll was made (between 1 and 10)
     * @exception IllegalArgumentException thrown if more pins were knocked down then there were standing at the start
     */
    public AbstractResult(int knockedDownPins, int startNrOfPins) {
        if(knockedDownPins > startNrOfPins) {
            throw new IllegalArgumentException("Cant knock down more pins then there are standing at the start");
        }
        this.startPins = startNrOfPins;
        this.knockedDownPins = knockedDownPins;
    }

    @Override
    public int getRemainingPins() {
        return this.startPins - this.knockedDownPins;
    }

    @Override
    public int getKnockedDownPins() {
        return this.knockedDownPins;
    }

}
