package prants.bowlingScore.result;

/**
 * Interface for result classes that represent one bowling throws result
 */
public interface IResult {

    /**
     * Getter for pins that remained standing after this result was achieved
     * @return int value of pins left standing, can be between 0 to 10
     */
    public int getRemainingPins();

    /**
     * Getter for how many pins were knocked down with this throw
     * @return int value of knocked down pins, can be between 0 to 10
     */
    public int getKnockedDownPins();

}
