package prants.bowlingScore.result;

/**
 * Standard result class, extends AbstractResult class,
 * represents the result of one throw in a game of bowling
 * How many pins there were, how many were knocked down and how many remain
 */
public class Result extends AbstractResult {

    /**
     * Constructor that expects there were 10 pins standing before the throw was made
     * @param knockedDownPins int value for how many pins were knocked down with this throw
     */
    public Result(int knockedDownPins) {
        super(knockedDownPins, 10);
    }

    /**
     * Constructor
     * @param knockedDownPins int value for how many pins were knocked down with this throw
     * @param startNrOfPins int value for how many pins were standing before this throw was made
     */
    public Result(int knockedDownPins, int startNrOfPins) {
        super(knockedDownPins, startNrOfPins);
    }

}
