package prants.bowlingScore;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import prants.gui.screen.ScreenStarter;

@SpringBootApplication
public class BowlingScoreApplication {

	public static void main(String[] args) {
		//SpringApplication.run(BowlingScoreApplication.class, args);
		var ctx = new SpringApplicationBuilder(BowlingScoreApplication.class).headless(false).run(args);
		ScreenStarter newGui = new ScreenStarter();
		newGui.startGUI();
	}

}
