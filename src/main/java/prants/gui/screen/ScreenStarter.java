package prants.gui.screen;

import prants.bowlingScore.game.Game;
import prants.bowlingScore.game.IGame;
import prants.bowlingScore.player.Player;
import prants.gui.screen.activePlayer.ActivePlayerManager;
import prants.gui.screen.leaderboard.LeaderboardManager;
import prants.gui.screen.playerAdding.PlayerAddingManager;
import prants.gui.screen.playerListDisplaying.PlayerListDisplay;
import prants.gui.screen.playerProgress.PlayerProgressManager;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.concurrent.Flow;

public class ScreenStarter {
    private IGame bowlingGame;
    private PlayerAddingManager playerAdding;
    private PlayerListDisplay playerListDisplay;
    private ActivePlayerManager activePlayerManager;
    private PlayerProgressManager playerProgressManager;
    private LeaderboardManager leaderboardManager;

    /**
     * Command for starting GUI
     */
    public void startGUI() {
        JFrame frame = new JFrame("Bowling GUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 800);

        this.makeBowlingGame();
        this.makePlayerAdding();
        this.makePlayerListDisplay();
        this.makeActivePlayerManager();
        this.makePlayerProgressManager();
        this.makeLeaderboardManager();

        JPanel panel = makeJPanel();
        panel.add(this.playerAdding.getPanel(), BorderLayout.NORTH);
        panel.add(this.playerListDisplay.getPanel(), BorderLayout.WEST);
        panel.add(this.activePlayerManager.getPanel(), BorderLayout.CENTER);
        panel.add(this.playerProgressManager.getPanel(), BorderLayout.EAST);
        panel.add(this.leaderboardManager.getPanel(),BorderLayout.SOUTH);
        frame.add(panel);
        frame.setVisible(true);
    }

    private void makeBowlingGame() {
        this.bowlingGame = new Game();
    }

    private JPanel makeJPanel() {
        return new JPanel(new BorderLayout());
    }

    private void makePlayerAdding() {
        this.playerAdding = new PlayerAddingManager(this.bowlingGame, this);
    }

    private void makePlayerListDisplay() {
        this.playerListDisplay = new PlayerListDisplay(this.bowlingGame, this);
    }

    private void makeActivePlayerManager() {
        this.activePlayerManager = new ActivePlayerManager(this.bowlingGame, this);
    }

    private void makePlayerProgressManager() {
        this.playerProgressManager = new PlayerProgressManager(this.bowlingGame, this);
    }

    private void makeLeaderboardManager() {
        this.leaderboardManager = new LeaderboardManager(this.bowlingGame, this);
    }

    public PlayerAddingManager getPlayerAdding() {
        return playerAdding;
    }

    public PlayerListDisplay getPlayerListDisplay() {
        return playerListDisplay;
    }

    public ActivePlayerManager getActivePlayerManager() {
        return activePlayerManager;
    }

    public PlayerProgressManager getPlayerProgressManager() {
        return this.playerProgressManager;
    }

    public LeaderboardManager getLeaderboardManager() {
        return this.leaderboardManager;
    }


}
