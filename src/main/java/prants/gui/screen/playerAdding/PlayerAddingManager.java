package prants.gui.screen.playerAdding;

import prants.bowlingScore.game.IGame;
import prants.gui.screen.ScreenStarter;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class PlayerAddingManager {
    private JPanel elementPanel;
    private IGame bowlingGame;
    private ScreenStarter screenStarter;
    private JTextField textField;
    private JButton addPlayer;
    private ArrayList<INewPlayerListener> listeners;

    public PlayerAddingManager(IGame bowlingGame, ScreenStarter screenStarter) {
        this.bowlingGame = bowlingGame;
        this.screenStarter = screenStarter;
        this.elementPanel = new JPanel(new FlowLayout());
        this.listeners = new ArrayList<>();
        this.makeTextField();
        this.makeButton();
    }

    private void makeTextField() {
        this.textField = new JTextField(20);
        this.elementPanel.add(this.textField);
    }

    private void makeButton() {
        this.addPlayer = new JButton("Enter new player with name");
        this.addPlayer.addActionListener(new PlayerAddingButtonListener(this));
        this.elementPanel.add(this.addPlayer);
    }

    public JPanel getPanel() {
        return this.elementPanel;
    }

    private String getPlayerName() {
        String writtenText = this.textField.getText();
        this.textField.setText("");
        return writtenText;
    }

    private void addPlayer(String name) {
        boolean addResult = this.bowlingGame.addPlayer(name);
        if(addResult) {
            informNewPlayerListeners();
        }
    }

    public void buttonReacting() {
        this.addPlayer(this.getPlayerName());
    }

    private void informNewPlayerListeners() {
        for(INewPlayerListener oneListener : this.listeners) {
            oneListener.newPlayerAdded();
        }
    }

    public void addNewPlayerListener(INewPlayerListener newListener) {
        this.listeners.add(newListener);
    }
}
