package prants.gui.screen.playerAdding;

public interface INewPlayerListener {

    public void newPlayerAdded();
}
