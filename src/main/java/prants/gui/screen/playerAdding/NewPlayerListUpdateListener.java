package prants.gui.screen.playerAdding;

import prants.gui.screen.playerListDisplaying.PlayerListDisplay;

public class NewPlayerListUpdateListener implements INewPlayerListener {
    private PlayerListDisplay playerListDisplay;

    public NewPlayerListUpdateListener(PlayerListDisplay playerListDisplay) {
        this.playerListDisplay = playerListDisplay;
    }

    @Override
    public void newPlayerAdded() {
        this.playerListDisplay.callRefreshOnPlayerList();
    }
}
