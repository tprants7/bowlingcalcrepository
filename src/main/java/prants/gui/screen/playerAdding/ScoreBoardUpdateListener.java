package prants.gui.screen.playerAdding;

import prants.gui.screen.leaderboard.LeaderboardManager;

public class ScoreBoardUpdateListener implements INewPlayerListener {
    private LeaderboardManager leaderboardManager;

    public ScoreBoardUpdateListener(LeaderboardManager manager) {
        this.leaderboardManager = manager;
    }

    @Override
    public void newPlayerAdded() {
        this.leaderboardManager.updateData();
    }
}
