package prants.gui.screen.playerAdding;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayerAddingButtonListener implements ActionListener {
    private PlayerAddingManager playerAddingManager;

    public PlayerAddingButtonListener(PlayerAddingManager playerAddingManager) {
        this.playerAddingManager = playerAddingManager;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.playerAddingManager.buttonReacting();
    }
}
