package prants.gui.screen.playerProgress;

import prants.bowlingScore.frame.IFrameCurrentState;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class OneFrameProgressPanel {
    private JPanel elementPanel;
    private JLabel frameNumber;
    private JLabel rollValues;
    private JLabel frameScore;

    public OneFrameProgressPanel() {
        this.elementPanel = new JPanel();
        BoxLayout newBoxLayout = new BoxLayout(this.elementPanel, BoxLayout.Y_AXIS);
        this.elementPanel.setLayout(newBoxLayout);
        this.elementPanel.setBorder(new EmptyBorder(new Insets(5, 0, 5, 0)));

        this.frameNumber = new JLabel("");
        this.elementPanel.add(this.frameNumber);

        this.rollValues = new JLabel("");
        this.elementPanel.add(this.rollValues);

        this.frameScore = new JLabel("");
        this.elementPanel.add(this.frameScore);
    }

    public void fillWithData(IFrameCurrentState frameState) {
        this.frameNumber.setText("Frame nr: "+frameState.getFrameNr());
        this.writeRollValues(frameState);
        this.frameScore.setText("Frame total: "+frameState.getCurrentFrameScore());
    }

    private void writeRollValues(IFrameCurrentState frameState) {
        String finalString = "| ";
        for(int oneResult : frameState.getRollValues()) {
            finalString = finalString + oneResult + " | ";
        }
        this.rollValues.setText(finalString);
    }

    public JPanel getPanel() {
        return this.elementPanel;
    }
}
