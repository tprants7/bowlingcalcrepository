package prants.gui.screen.playerProgress;

import prants.bowlingScore.game.IGame;
import prants.bowlingScore.soloGame.IPlayerGameCurrentState;
import prants.gui.screen.ScreenStarter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerProgressManager {
    private IGame bowlingGame;
    private ScreenStarter screenStarter;
    private JPanel elementPanel;
    private List<OneFrameProgressPanel> frameProgressPanelList;

    public PlayerProgressManager(IGame bowlingGame, ScreenStarter screenStarter) {
        this.bowlingGame = bowlingGame;
        this.screenStarter = screenStarter;
        this.makeFrameProgressPanelList();
        this.makeSelfPanel();
    }

    private void makeFrameProgressPanelList() {
        this.frameProgressPanelList = new ArrayList<>();
        for(int i=0; i < 10; i++) {
            this.frameProgressPanelList.add(new OneFrameProgressPanel());
        }
    }

    private void makeSelfPanel() {
        this.elementPanel = new JPanel();
        BoxLayout newBoxLayout = new BoxLayout(this.elementPanel, BoxLayout.Y_AXIS);
        this.elementPanel.setLayout(newBoxLayout);
        this.elementPanel.setBorder(new EmptyBorder(new Insets(10, 20, 10, 20)));
        for(OneFrameProgressPanel oneProgressPanel : this.frameProgressPanelList) {
            this.elementPanel.add(oneProgressPanel.getPanel());
        }
    }

    public void callUpdateForPlayerNr(int nr) {
        this.updateData(this.bowlingGame.getCurrentStateDescriptionForPlayer(nr));
    }

    private void updateData(IPlayerGameCurrentState currentState) {
        if(currentState.getFrameStates().size() != 10) {
            System.out.println("something is wrong, player game current state does not have 10 frames, it has "+currentState.getFrameStates().size());
        }
        for(int i=0; i < 10; i++) {
            this.frameProgressPanelList.get(i).fillWithData(currentState.getFrameStates().get(i));
        }

    }

    public JPanel getPanel() {
        return this.elementPanel;
    }


}
