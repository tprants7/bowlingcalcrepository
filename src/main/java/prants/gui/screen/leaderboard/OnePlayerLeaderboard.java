package prants.gui.screen.leaderboard;

import prants.bowlingScore.soloGame.IPlayerGameCurrentState;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class OnePlayerLeaderboard {
    private JPanel elementPanel;
    private JLabel playerName;
    private JLabel currentScore;
    private JLabel progress;

    public OnePlayerLeaderboard() {
        this.elementPanel = new JPanel();
        BoxLayout newBoxLayout = new BoxLayout(this.elementPanel, BoxLayout.Y_AXIS);
        this.elementPanel.setLayout(newBoxLayout);
        this.elementPanel.setBorder(new EmptyBorder(new Insets(10, 0, 10, 40)));

        this.playerName = new JLabel("");
        this.elementPanel.add(this.playerName);

        this.currentScore = new JLabel("");
        this.elementPanel.add(this.currentScore);

        this.progress = new JLabel("");
        this.elementPanel.add(this.progress);
    }

    public void fillWithData(IPlayerGameCurrentState currentState) {
        this.playerName.setText(currentState.getPlayerNickname());
        this.currentScore.setText("Score: "+currentState.getCurrentRunningTotalScore());
        if(currentState.isFinishedPlaying()) {
            this.progress.setText("Finished");
        }
        else {
            this.progress.setText("On frame: "+currentState.getCurrentFrameNr());
        }
    }

    public JPanel getPanel() {
        return this.elementPanel;
    }

}
