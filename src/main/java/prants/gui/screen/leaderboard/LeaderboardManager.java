package prants.gui.screen.leaderboard;

import prants.bowlingScore.game.IGame;
import prants.bowlingScore.soloGame.IPlayerGameCurrentState;
import prants.gui.screen.ScreenStarter;
import prants.gui.screen.playerAdding.ScoreBoardUpdateListener;
import prants.gui.screen.playerProgress.OneFrameProgressPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class LeaderboardManager {
    private IGame bowlingGame;
    private ScreenStarter screenStarter;
    private JPanel elementPanel;
    private int amountOfScoresShown;
    private List<OnePlayerLeaderboard> leaderboardElements;

    public LeaderboardManager(IGame bowlingGame, ScreenStarter screenStarter) {
        this.bowlingGame = bowlingGame;
        this.screenStarter = screenStarter;
        this.amountOfScoresShown = 9;
        this.makeFrameProgressPanelList();
        this.makeSelfPanel();
        setUpPlayerAddListener();
    }

    private void makeFrameProgressPanelList() {
        this.leaderboardElements = new ArrayList<>();
        for(int i=0; i < amountOfScoresShown; i++) {
            this.leaderboardElements.add(new OnePlayerLeaderboard());
        }
    }

    private void makeSelfPanel() {
        this.elementPanel = new JPanel();
        BoxLayout newBoxLayout = new BoxLayout(this.elementPanel, BoxLayout.X_AXIS);
        this.elementPanel.setLayout(newBoxLayout);
        this.elementPanel.setBorder(new EmptyBorder(new Insets(10, 20, 10, 20)));
        for(OnePlayerLeaderboard oneElement : this.leaderboardElements) {
            this.elementPanel.add(oneElement.getPanel());
        }
    }

    private void setUpPlayerAddListener() {
        this.screenStarter.getPlayerAdding().addNewPlayerListener(new ScoreBoardUpdateListener(this));
    }

    public void updateData() {
        this.writeScoreValues(this.getCurrentStates());
    }

    private List<IPlayerGameCurrentState> getCurrentStates() {
        return this.bowlingGame.getAllPlayersCurrentStates();
    }

    private void writeScoreValues(List<IPlayerGameCurrentState> stateList) {
        int maxIterations = this.amountOfScoresShown;
        if(stateList.size() < this.amountOfScoresShown) {
            maxIterations = stateList.size();
        }
        for(int i = 0; i < maxIterations; i++) {
            this.leaderboardElements.get(i).fillWithData(stateList.get(i));
        }
    }

    public JPanel getPanel() {
        return this.elementPanel;
    }
}
