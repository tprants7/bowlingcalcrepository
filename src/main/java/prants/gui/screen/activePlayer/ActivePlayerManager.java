package prants.gui.screen.activePlayer;

import prants.bowlingScore.game.IGame;
import prants.gui.screen.ScreenStarter;
import prants.gui.screen.playerListDisplaying.PlayerSelectListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ActivePlayerManager {
    private IGame bowlingGame;
    private ScreenStarter screenStarter;
    private JPanel elementPanel;
    private PlayerSelectListener playerSelectListener;
    private int activePlayerId;
    private JLabel playerNameLabel;
    private JLabel currentRollingSituation;

    private RollAddingElement rollAddingElement;

    public ActivePlayerManager(IGame bowlingGame, ScreenStarter screenStarter) {
        this.bowlingGame = bowlingGame;
        this.screenStarter = screenStarter;
        this.activePlayerId = -1;
        this.makePlayerSelectingListener();
        this.makeJPanelElements();
    }

    public void callSelectionChange() {
        this.setActiveChar(this.screenStarter.getPlayerListDisplay().getSelectedString());
        this.updateDisplay();
        this.screenStarter.getPlayerProgressManager().callUpdateForPlayerNr(this.activePlayerId);
    }

    private void setActiveChar(String nickname) {
        this.activePlayerId = this.bowlingGame.getPlayerId(nickname);
    }

    private void makePlayerSelectingListener() {
        this.playerSelectListener = new PlayerSelectListener(this);
        this.screenStarter.getPlayerListDisplay().addSelectionChangeListener(this.playerSelectListener);
    }

    private void makeJPanel() {
        this.elementPanel = new JPanel();
        BoxLayout newBoxLayout = new BoxLayout(this.elementPanel, BoxLayout.PAGE_AXIS);
        this.elementPanel.setLayout(newBoxLayout);
        this.elementPanel.setBorder(new EmptyBorder(new Insets(50, 20, 5, 20)));
    }

    private void makeJPanelElements() {
        this.makeJPanel();
        this.playerNameLabel = new JLabel("Active Player: ---, Id: -");
        this.elementPanel.add(this.playerNameLabel);
        this.currentRollingSituation = new JLabel("Can roll: false, current frame: --, pins standing: --, running score: --");
        this.elementPanel.add(this.currentRollingSituation);
        this.makeRollAddingElement();
    }

    private void makeRollAddingElement() {
        this.rollAddingElement = new RollAddingElement(this);
        this.elementPanel.add(this.rollAddingElement.getPanel());
    }

    private void updateDisplay() {
        this.playerNameLabel.setText("Active Player: "+this.bowlingGame.getPlayerNickname(this.activePlayerId)+", Id: "+this.activePlayerId);
        this.currentRollingSituation.setText("Can roll: "+this.bowlingGame.canRollForPlayer(this.activePlayerId)+
                ", current frame: "+this.bowlingGame.getPlayerCurrentFrame(this.activePlayerId)+
                ", pins standing: "+this.bowlingGame.getCurrentlyStandingPinsForPlayer(this.activePlayerId)+
                ", running score: "+this.bowlingGame.getRunningScoreForPlayer(this.activePlayerId));
    }

    private boolean hasActivePlayer() {
        return this.activePlayerId != -1;
    }

    public JPanel getPanel() {
        return this.elementPanel;
    }

    public void tryToAddARollToPlayer(int rollValue) {
        if(this.hasActivePlayer()) {
            try {
                this.bowlingGame.addPointsForPlayer(this.activePlayerId, rollValue);
                this.updateDisplay();
                this.screenStarter.getPlayerProgressManager().callUpdateForPlayerNr(this.activePlayerId);
                this.screenStarter.getLeaderboardManager().updateData();
            }
            catch (Exception e) {
                System.out.println("Wasn't able to add new roll");
            }
        }
    }

}
