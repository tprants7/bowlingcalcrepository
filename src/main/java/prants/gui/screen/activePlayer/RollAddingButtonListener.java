package prants.gui.screen.activePlayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RollAddingButtonListener implements ActionListener {
    private RollAddingElement rollAddingElement;

    public RollAddingButtonListener(RollAddingElement rollAddingElement) {
        this.rollAddingElement = rollAddingElement;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.rollAddingElement.extractInsertedValue();
    }
}
