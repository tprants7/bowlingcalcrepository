package prants.gui.screen.activePlayer;

import prants.bowlingScore.game.IGame;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RollAddingElement {
    private JPanel elementPanel;
    private ActivePlayerManager activePlayerManager;
    private JLabel knockedDownPinsLabel;
    private JTextField input;
    private JButton insertButton;

    public RollAddingElement(ActivePlayerManager activePlayerManager) {
        this.activePlayerManager = activePlayerManager;
        this.makePanel();
        this.makeElements();
        this.makeListener();
    }

    private void makePanel() {
        this.elementPanel = new JPanel();
        BoxLayout newBoxLayout = new BoxLayout(this.elementPanel, BoxLayout.X_AXIS);
        this.elementPanel.setLayout(newBoxLayout);
        this.elementPanel.setMaximumSize(new Dimension(500, 30));
    }

    private void makeElements() {
        this.knockedDownPinsLabel = new JLabel("New throw (nr of pins knocked down): ");
        this.elementPanel.add(this.knockedDownPinsLabel);

        this.input = new JTextField(3);
        this.elementPanel.add(this.input);

        this.insertButton = new JButton("Add new score");
        this.elementPanel.add(this.insertButton);
    }

    private void makeListener() {
        this.insertButton.addActionListener(new RollAddingButtonListener(this));
    }

    public JPanel getPanel() {
        return this.elementPanel;
    }

    public void extractInsertedValue() {
        int translatedValue = tryToTurnIntoNumber(this.getInsertedText());
        if(translatedValue != -1) {
            this.activePlayerManager.tryToAddARollToPlayer(translatedValue);
        }
    }

    private String getInsertedText() {
        String writtenText = this.input.getText();
        this.input.setText("");
        return writtenText;
    }

    private int tryToTurnIntoNumber(String inputtedText) {
        try {
            int number = Integer.parseInt(inputtedText);
            return number;
        }
        catch (Exception e) {
            System.out.println("Wrong input");
        }
        return -1;
    }

}
