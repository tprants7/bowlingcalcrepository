package prants.gui.screen.playerListDisplaying;

import prants.gui.screen.ScreenStarter;
import prants.gui.screen.activePlayer.ActivePlayerManager;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PlayerSelectListener implements ListSelectionListener {
    private ActivePlayerManager activePlayerManager;

    public PlayerSelectListener(ActivePlayerManager activePlayerManager) {
        this.activePlayerManager = activePlayerManager;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(!e.getValueIsAdjusting()) {
            activePlayerManager.callSelectionChange();
        }
    }
}
