package prants.gui.screen.playerListDisplaying;

import prants.bowlingScore.game.IGame;
import prants.gui.screen.ScreenStarter;
import prants.gui.screen.playerAdding.INewPlayerListener;
import prants.gui.screen.playerAdding.NewPlayerListUpdateListener;
import prants.gui.screen.playerAdding.PlayerAddingManager;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.List;

public class PlayerListDisplay {
    //private JPanel elementPanel;
    private JScrollPane listScroller;
    private IGame bowlingGame;
    private ScreenStarter screenStarter;
    private DefaultListModel<String> playerList;
    private PlayerAddingManager playerAddingManager;
    private INewPlayerListener listener;
    private JList listElement;

    public PlayerListDisplay(IGame bowlingGame, ScreenStarter screenStarter) {
        this.bowlingGame = bowlingGame;
        this.screenStarter = screenStarter;
        this.setUpListModel();
        this.playerAddingManager = screenStarter.getPlayerAdding();
        this.setUpGraphicList();
        this.setUpListener();
    }

    private void setUpListModel() {
        this.playerList = new DefaultListModel<String>();
    }

    private void setUpListener() {
        this.listener = new NewPlayerListUpdateListener(this);
        this.playerAddingManager.addNewPlayerListener(this.listener);
    }

    public void callRefreshOnPlayerList() {
        this.addLatestPlayerFromGame();
    }

    private void addLatestPlayerFromGame() {
        List<String> names = this.bowlingGame.getNamesOfAllThePlayers();
        if(names.size() > this.playerList.size()) {
            this.playerList.addElement(names.get(names.size()-1));
        }
    }

    private void setUpGraphicList() {
        this.listElement = new JList(this.playerList);
        this.listElement.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        this.listElement.setLayoutOrientation(JList.VERTICAL);
        this.listElement.setVisibleRowCount(-1);
        this.listScroller = new JScrollPane(this.listElement);
        listScroller.setPreferredSize(new Dimension(250, 80));
    }

    public JScrollPane getPanel() {
        return this.listScroller;
    }

    public void addSelectionChangeListener(ListSelectionListener newListener) {
        this.listElement.addListSelectionListener(newListener);
    }

    public String getSelectedString() {
        return this.listElement.getSelectedValue().toString();
    }



}
